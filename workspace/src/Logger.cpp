#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include<string>
#include<iostream>


int main(){

int fd;
char buffer[128];

	// CREACION FIFO
	if (mkfifo("/tmp/FIFO", 0600)<0) {
		perror("Error al crear el FIFO");
		return 1;
	}
		//APERTURA FIFO	
	if ((fd=open("/tmp/FIFO", O_RDONLY))<0) {
		perror("Error al abrir el FIFO");
		return 1;
	}
	
	while (read(fd, buffer, 128))  {
		printf("%s",buffer);
		
	}
	close(fd);
	unlink("/tmp/FIFO");
	return(0);

}
