# Changelog
Todos los cambios descables aparecerán aquí.

## [v1.3]
### Añadido
-Logger.
-Bot.
-Comunicación a traves de memoria compartida y fifos.


## [v1.2]
### Añadido
-Movimiento tanto de la esfera como de las raquetas de juego.
-Disminución del tamaño de la esfera con el paso del tiempo.
-Etiqueta que indica el final de la práctica 2.


## [v1.1]
### Añadido
-Identificaciones de los autores en los ficheros de cabecera.
-Etiqueta que indica el punto final de la práctica 1.
